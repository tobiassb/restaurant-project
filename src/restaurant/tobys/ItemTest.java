package restaurant.tobys;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ItemTest {
    @Test
    public void item_has_name_and_price() {
        Item item = new Item("Fish & Chips", 3.50, 1);
        assertEquals("Fish & Chips", item.getDish());
        assertEquals(3.50, item.getPrice(), 0);
    }

    @Test
    public void can_change_price() {
        Item item = new Item("Fish & Chips", 3.50, 1);
        assertEquals("Fish & Chips", item.getDish());
        assertEquals(3.50, item.getPrice(), 0);
        item.changePrice(2.50);
        assertEquals(2.50, item.getPrice(), 0);
    }
    
}
