package restaurant.tobys;
import static org.junit.Assert.*;


import org.junit.Test;

public class RestaurantTest {
    @Test
    public void restaurant_has_name_and_URL() {
        Restaurant restaurant = new Restaurant("The Grand", "https://4.bp.blogspot.com/-um402u5nYa8/Uubx36SwVEI/AAAAAAAASII/WtQUX8jxXeY/s1600/_MG_9174.JPG");
        assertEquals("The Grand", restaurant.getName());
        assertEquals("https://4.bp.blogspot.com/-um402u5nYa8/Uubx36SwVEI/AAAAAAAASII/WtQUX8jxXeY/s1600/_MG_9174.JPG", restaurant.getImageURL());
    }

    @Test
    public void has_menus() {
        Restaurant restaurant = new Restaurant("The Grand", "https://4.bp.blogspot.com/-um402u5nYa8/Uubx36SwVEI/AAAAAAAASII/WtQUX8jxXeY/s1600/_MG_9174.JPG");
        assertNotNull(restaurant.getMenus());
    }

    @Test
    public void has_menus_mains() {
        Restaurant restaurant = new Restaurant("The Grand", "https://4.bp.blogspot.com/-um402u5nYa8/Uubx36SwVEI/AAAAAAAASII/WtQUX8jxXeY/s1600/_MG_9174.JPG");
        Menu menu = new Menu("Mains", 1);
        restaurant.getMenus().add(menu);
        assertEquals(menu, restaurant.getMenus().get(0));
    }

    @Test
    public void can_link_all() {
        Restaurant restaurant = new Restaurant("The Grand", "https://4.bp.blogspot.com/-um402u5nYa8/Uubx36SwVEI/AAAAAAAASII/WtQUX8jxXeY/s1600/_MG_9174.JPG");
        restaurant.getMenus().add(new Menu("Mains", 1));
        restaurant.getMenus().get(0).getItems().add(new Item("Fish & Chips", 2.50, 1));
        assertEquals("Fish & Chips", restaurant.getMenus().get(0).getItems().get(0).getDish());
    }
}
