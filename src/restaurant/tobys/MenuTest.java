package restaurant.tobys;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class MenuTest {
    @Test
    public void menu_has_name() {
        Menu menu = new Menu("Mains", 1);
        assertEquals("Mains", menu.getName());
    }

    @Test
    public void menu_has_items() {
        Menu menu = new Menu("Mains", 1);
        assertNotNull(menu.getItems());
    }
    
}
