package restaurant.tobys;
public class Main {
    public static void main(String[] args) throws Exception {
        new DB("jdbc:sqlite:./sql.db/restaurants.db");
        Restaurant.init();
        Menu.init();
        Item.init();
        DB.conn.close();
    }
}
