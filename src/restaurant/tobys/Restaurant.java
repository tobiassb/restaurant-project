package restaurant.tobys;
import java.sql.*;
import java.util.ArrayList;

public class Restaurant {
    private int id;
    private String name;
    private String imageURL;
    private ArrayList<Menu> menus;

    public static ArrayList<Restaurant> all = new ArrayList<>();

    public static void init() {
        try {
            Statement createRestaurant = DB.conn.createStatement();
            createRestaurant.execute("CREATE TABLE IF NOT EXISTS restaurants (id INTEGER PRIMARY KEY, name TEXT, imageURL TEXT);");
            Statement getRestaurant = DB.conn.createStatement();
            ResultSet restaurants = getRestaurant.executeQuery("SELECT * FROM restaurants;");
            while(restaurants.next()) {
                int id = restaurants.getInt(1);
                String name = restaurants.getString(2);
                String imageURL = restaurants.getString(3);
                new Restaurant(id, name, imageURL);
            }
        } catch (Exception errr) {
            System.out.println(errr.getMessage());
        }
    }

    public Restaurant(String name, String imageURL) {
        this.name = name;
        this.imageURL = imageURL;
        this.menus = new ArrayList<Menu>();
        try {
            PreparedStatement insertRestaurant = DB.conn.prepareStatement("INSERT INTO restaurants (name, imageURL) VALUES (?,?);");
            insertRestaurant.setString(1, this.name);
            insertRestaurant.setString(2, this.imageURL);
            insertRestaurant.executeUpdate();
            this.id = insertRestaurant.getGeneratedKeys().getInt(1);
        } catch (SQLException errr) {
            System.out.println(errr.getMessage());
        }
        Restaurant.all.add(this);
    }

    public Restaurant(int id, String name, String imageURL) {
        this.id = id;
        this.name = name;
        this.imageURL = imageURL;
        Restaurant.all.add(this);
    }


    public int getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getImageURL() {
        return this.imageURL;
    }

    public ArrayList<Menu> getMenus() {
        return this.menus;
    }

    public void createMenu(String title) {
        Menu menu = new Menu(title, this.id);
        this.menus.add(menu);
    }

}
