package restaurant.tobys;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Item {
    private int id;
    private int menuID;
    private String dish;
    private double price;

    public static ArrayList<Item> all = new ArrayList<>();

    public static void init() {
        try {
            Statement createItem = DB.conn.createStatement();
            createItem.execute("CREATE TABLE IF NOT EXISTS items (id INTEGER PRIMARY KEY, menuID INTEGER, dish TEXT, price DOUBLE);");
            Statement getMenu = DB.conn.createStatement();
            ResultSet item = getMenu.executeQuery("SELECT * FROM items;");
            while (item.next()) {
                int id = item.getInt(1);
                int menuID = item.getInt(2);
                String dish = item.getString(3);
                double price = item.getDouble(4);
                new Item(id, menuID, dish, price);
                }
        } catch (Exception errr) {
            System.out.println(errr.getMessage());
        }
    }

    public Item(String dish, double price, int menuID) {
        this.dish = dish;
        this.price = price;
        this.menuID = menuID;
        try {
            PreparedStatement insertItem = DB.conn.prepareStatement("INSERT INTO items (menuID, dish, price) VALUES (?,?,?);");
            insertItem.setInt(1, this.menuID);
            insertItem.setString(2, this.dish);
            insertItem.setDouble(3, this.price);
            insertItem.executeUpdate();
            this.id = insertItem.getGeneratedKeys().getInt(1);
            System.out.println(this.id);
            Item.all.add(this);
        } catch (SQLException errr) {
            System.out.println(errr.getMessage());
        }
    }

    public Item(int id,int menuID,String dish,double price) {
        this.id = id;
        this.menuID = menuID;
        this.dish = dish;
        this.price = price;
        Item.all.add(this);
    }

    public String getDish() {
        return this.dish;
    }

    public double getPrice() {
        return this.price;
    }

    public double changePrice(double price) {
        return this.price = price;
    }

    public int getId() {
        return this.id;
    }

    public int getMenuID() {
        return this.menuID;
    }

}
