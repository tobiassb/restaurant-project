package restaurant.tobys;
import java.sql.*;
import java.util.ArrayList;

public class Menu {
    private int id;
    private int restaurantID;
    private String name;
    private ArrayList<Item> items;

    public static ArrayList<Menu> all = new ArrayList<>();

    public static void init() {
        try {
            Statement createMenu = DB.conn.createStatement();
            createMenu.execute("CREATE TABLE IF NOT EXISTS menus (id INTEGER PRIMARY KEY, restaurantID INTEGER, menu TEXT);");
            Statement getMenu = DB.conn.createStatement();
            ResultSet Menu = getMenu.executeQuery("SELECT * FROM menus;");
            while(Menu.next()) {
                int id = Menu.getInt(1);
                int restaurantID = Menu.getInt(2);
                String name = Menu.getString(3);
                new Menu(id, restaurantID, name);
            }
        } catch (Exception errr) {
            System.out.println(errr.getMessage());
        }
    }

    public Menu(String title, int restaurantID) {
        this.name = title;
        this.restaurantID = restaurantID;
        this.items = new ArrayList<Item>();
        try {
            PreparedStatement insertMenu = DB.conn.prepareStatement("INSERT INTO menus (restaurantID, menu) VALUES (?,?);");
            insertMenu.setInt(1, this.restaurantID);
            insertMenu.setString(2, this.name);
            insertMenu.executeUpdate();
            this.id = insertMenu.getGeneratedKeys().getInt(1);
            Menu.all.add(this);
        } catch (SQLException errr) {
            System.out.println(errr.getMessage());
        }
    }

    public Menu(int id,int restaurantID,String name) {
        this.id = id;
        this.restaurantID = restaurantID;
        this.name = name;
        Menu.all.add(this);
    }

    public String getName() {
        return this.name;
    }

    public int getRestaurantID() {
        return this.restaurantID;
    }

    public ArrayList<Item> getItems() {
        return this.items;
    }

    public int getID() {
        return this.id;
    }

    public void createItem(String dish, double price) {
        Item item = new Item(dish, price, this.id);
        this.items.add(item);
    }

}
