package restaurant.tobys;
import java.sql.*;

public class DB {
    public static Connection conn;

    public DB(String connectionString) {
        try {
            DB.conn = DriverManager.getConnection(connectionString);
        } catch (SQLException errr) {
            System.out.println(errr.getMessage());
        }
    }

    public static void close() {
        try {
            DB.conn.close();
        } catch (SQLException errr) {
            System.out.println(errr.getMessage());
        }
    }

}
